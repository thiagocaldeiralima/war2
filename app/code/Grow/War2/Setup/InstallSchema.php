<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */
namespace Grow\War2\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $conn = $setup->getConnection();
        $tableName = $setup->getTable('war_game_history');
        if ($conn->isTableExists($tableName) != true) {
            $table = $conn->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true]
                )
                ->addColumn(
                    'content',
                    Table::TYPE_TEXT,
                    '2M',
                    ['nullbale' => false, 'default' => '']
                )
                ->setOption('charset', 'utf8');
            $conn->createTable($table);
        }

        $tableName = $setup->getTable('war_player');
        if ($conn->isTableExists($tableName) != true) {
            $table = $conn->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true]
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false, 'default' => '']
                )
                ->addColumn(
                    'hash',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false, 'default' => '']
                )
                ->addColumn(
                    'content',
                    Table::TYPE_TEXT,
                    '2M',
                    ['nullbale' => false, 'default' => '']
                )
                ->addColumn(
                    'is_active',
                    Table::TYPE_BOOLEAN,
                    1,
                    ['default' => 0],
                    'active playing'
                )
                ->addColumn(
                    'ordering',
                    Table::TYPE_BOOLEAN,
                    1,
                    [],
                    'Type 1 = objective, 2 Territory'
                )
                ->setOption('charset', 'utf8');
            $conn->createTable($table);
        }

        $tableName = $setup->getTable('war_card');
        if ($conn->isTableExists($tableName) != true) {
            $table = $conn->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true]
                )
                ->addColumn(
                    'image',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable'=>false,'default'=>'']
                )
                ->addColumn(
                    'is_used',
                    Table::TYPE_BOOLEAN,
                    1,
                    [],
                    'Is Used'
                )
                ->addColumn(
                    'type',
                    Table::TYPE_BOOLEAN,
                    1,
                    [],
                    'Type 1 = objective, 2 Territory'
                )
                ->setOption('charset', 'utf8');
            $conn->createTable($table);
        }
        $tableName = $setup->getTable('war_board');
        if ($conn->isTableExists($tableName) != true) {
            $table = $conn->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true]
                )
                ->addColumn(
                    'active_player_id',
                    Table::TYPE_INTEGER,
                    11,
                    []
                )
                ->addColumn(
                    'enemy_player_id',
                    Table::TYPE_INTEGER,
                    11,
                    []
                )
                ->setOption('charset', 'utf8');
            $conn->createTable($table);
        }
        $setup->endSetup();
    }
}
