<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */

namespace Grow\War2\Setup;

use Grow\War2\Model\Card;
use Grow\War2\Model\CardFactory;
use Grow\War2\Model\Player;
use Grow\War2\Model\PlayerFactory;
use Grow\War2\Model\Board;
use Grow\War2\Model\BoardFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var CardFactory
     */
    private $cardFactory;
    /**
     * @var PlayerFactory
     */
    private $playerFactory;
    /**
     * @var BoardFactory
     */
    private $boardFactory;

    public function __construct(
        CardFactory $cardFactory,
        PlayerFactory $playerFactory,
        BoardFactory $boardFactory
    ) {
        $this->cardFactory = $cardFactory;
        $this->playerFactory = $playerFactory;
        $this->boardFactory = $boardFactory;
    }

    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @SuppressWarnings("PHPMD.UnusedFormalParameter")
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        for ($i = 1; $i < 20; $i++) {
            /** @var Card $card */
            $card = $this->cardFactory->create();
            $card->setData([
                'image' => 'cards/objective-' . $i . '.png',
                'is_used' => 0,
                'type' => Card::TYPE_OBJECTIVE
            ]);
            $card->save();
        }

        for ($i = 1; $i < 61; $i++) {
            /** @var Card $card */
            $card = $this->cardFactory->create();
            $card->setData([
                'image' => 'cards/territory-' . $i . '.png',
                'is_used' => 0,
                'type' => Card::TYPE_TERRITORY
            ]);
            $card->save();
        }

        foreach ($this->getPlayers() as $playerData) {
            /** @var Player $player */
            $player = $this->playerFactory->create();
            $player->setData($playerData)->save();
        }

        $board = $this->boardFactory->create();
        $board->save();
        $setup->endSetup();
    }

    private function getPlayers()
    {
        return [
            ['hash' => 'KZ0IL224A7NI6R6ZYBMM', 'name' => 'Jack', 'ordering' => 1],
            ['hash' => '1GUNVEXHOYLDN66YYSG5', 'name' => 'Jane', 'ordering' => 2],
            ['hash' => '80W7N61MWPA9MQ3BHI2Q', 'name' => 'Peter', 'ordering' => 3],
            ['hash' => '48LIQV6N9M9FNXHNZ99D', 'name' => 'Ana', 'ordering' => 4],
            ['hash' => 'JZER2JIIM2ZZA6FO26YF', 'name' => 'Vanessa', 'ordering' => 5],
            ['hash' => '4HERTIZLH7WON5XHTJ3Y', 'name' => 'Jeffry', 'ordering' => 6],
        ];
    }
}
