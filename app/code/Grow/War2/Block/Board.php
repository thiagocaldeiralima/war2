<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */
namespace Grow\War2\Block;

use Grow\War2\Model\GameManager;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Session\SessionManagerInterface as Session;
use Magento\Store\Model\StoreManagerInterface;

class Board extends Template
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var Session
     */
    private $sessionManager;
    /**
     * @var GameManager
     */
    private $gameManager;

    public function __construct(
        StoreManagerInterface $storeManager,
        Template\Context $context,
        Session $sessionManager,
        GameManager $gameManager,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->storeManager = $storeManager;
        $this->sessionManager = $sessionManager;
        $this->gameManager = $gameManager;
    }

    public function getImageUrl($image)
    {
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . $image;
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getPlayerName()
    {
        $player = $this->sessionManager->getPlayer();
        if ($player) {
            return $player->getName();
        }
        return "";
    }

    public function getLoggedPlayer()
    {
        return $this->sessionManager->getPlayer();
    }

    public function getActivePlayer()
    {
        return $this->gameManager->getActivePlayer();
    }

    /**
     * @return array
     */
    public function getOtherPlayers()
    {
        return $this->gameManager->getOtherPlayers();
    }

    /**
     * @return string
     */
    public function getObjectiveImage()
    {
        $player = $this->sessionManager->getPlayer();
        $content = json_decode($player->getData('content') ?? '{}', true);

        if (isset($content['objective'])) {
            return $this->getImageUrl($content['objective']['image']);
        }
        return "";
    }

    public function getTerritoryCards()
    {
        $player = $this->sessionManager->getPlayer();
        $content = json_decode($player->getData('content') ?? '{}', true);
        if (isset($content['territory'])) {
            return $content['territory'];
        }
        return [];
    }

    /**
     * @return false|string
     */
    public function getActivePlayerData()
    {
        $activePlayer = $this->getActivePlayer();
        return json_encode(
            [
                'id' => $activePlayer->getId(),
                'name' => $activePlayer->getName()
            ]
        );
    }
}
