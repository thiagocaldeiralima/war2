<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */
namespace Grow\War2\Controller\Index;

use Grow\War2\Model\Player;
use Grow\War2\Model\PlayerManager;
use Grow\War2\Model\ResourceModel\Player\Collection;
use Grow\War2\Model\ResourceModel\Player\CollectionFactory;
use Magento\Framework\Session\SessionManagerInterface as Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class LoginPost extends Action
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var Session
     */
    private $session;
    /**
     * @var PlayerManager
     */
    private $playerManager;

    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        Session $session,
        PlayerManager $playerManager
    ) {
        parent::__construct($context);
        $this->collectionFactory = $collectionFactory;
        $this->session = $session;
        $this->playerManager = $playerManager;
    }

    public function execute()
    {
        if ($hash = $this->getRequest()->getParam('hash')) {
            /** @var Collection $collection */
            $collection = $this->collectionFactory->create();
            $collection->addFieldToFilter('hash', $hash);

            /** @var Player $player */
            $player = $collection->getFirstItem();
            if ($player->getId()) {
                $this->session->start();
                $this->playerManager->updateGameData($player);
                $this->session->setPlayer($player);
                return $this->_redirect('*/*/board');
            }
            $this->messageManager->addErrorMessage("Invalid Hash");
            return $this->_redirect('*/*/login');

        }
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}
