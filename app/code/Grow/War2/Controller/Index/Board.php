<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */
namespace Grow\War2\Controller\Index;

use Grow\War2\Model\GameManager;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Board extends \Magento\Framework\App\Action\Action
{
    /**
     * @var GameManager
     */
    private $manager;

    public function __construct(
        Context $context,
        GameManager $manager
    ) {
        parent::__construct($context);
        $this->manager = $manager;
    }

    public function execute()
    {
        if (!$this->manager->isLoggedIn()) {
           return $this->_redirect('*/*/login');
        }
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}
