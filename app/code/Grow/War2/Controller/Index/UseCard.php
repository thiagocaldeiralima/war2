<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */
namespace Grow\War2\Controller\Index;

use Grow\War2\Model\PlayerManager;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultFactory;

class UseCard extends Action
{

    /**
     * @var PlayerManager
     */
    private $playerManager;
    /**
     * @var JsonFactory
     */
    private $jsonResultFactory;

    public function __construct(
        Context $context,
        PlayerManager $playerManager,
        JsonFactory $jsonResultFactory
    ) {
        parent::__construct($context);
        $this->playerManager = $playerManager;
        $this->jsonResultFactory = $jsonResultFactory;
    }

    public function execute()
    {
        if ($cardIds = $this->getRequest()->getParam('usedCard')) {
            $this->playerManager->useTerritoryCards($cardIds);
        }

        $response = $this->jsonResultFactory->create();
        return $response->setData([
            'success' => true
        ]);
    }
}
