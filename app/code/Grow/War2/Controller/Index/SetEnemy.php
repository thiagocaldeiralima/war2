<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */
namespace Grow\War2\Controller\Index;

use Grow\War2\Model\GameManager;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class SetEnemy extends \Magento\Framework\App\Action\Action
{
    /**
     * @var GameManager
     */
    private $gameManager;
    /**
     * @var JsonFactory
     */
    private $jsonResultFactory;

    public function __construct(
        Context $context,
        GameManager $gameManager,
        JsonFactory $jsonResultFactory
    ) {
        parent::__construct($context);
        $this->gameManager = $gameManager;
        $this->jsonResultFactory = $jsonResultFactory;
    }

    public function execute()
    {
        $this->gameManager->setEnemy($this->getRequest()->getParam('enemyId'));
        $response = $this->jsonResultFactory->create();
        $response->setData(['enemy' => $this->gameManager->getEnemy()->getData()]);
        return $response;
    }
}
