<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */
namespace Grow\War2\Controller\Index;

use Grow\War2\Model\GameManager;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\HTTP\Client\Curl;

class Dices extends \Magento\Framework\App\Action\Action
{
    /**
     * @var JsonFactory
     */
    private $jsonResultFactory;
    /**
     * @var Curl
     */
    private $curl;
    /**
     * @var GameManager
     */
    private $gameManager;

    public function __construct(
        Context $context,
        JsonFactory $jsonResultFactory,
        Curl $curl,
        GameManager $gameManager

    ) {
        parent::__construct($context);
        $this->jsonResultFactory = $jsonResultFactory;
        $this->curl = $curl;
        $this->gameManager = $gameManager;
    }

    public function execute()
    {
        if (!$this->gameManager->isLoggedIn()) {
            $this->_redirect('*/*/login');
        }
        $this->curl->addHeader('Accept', 'application/json');
        $quantity = $this->getRequest()->getParam('q');

        $this->curl->get(sprintf('http://roll.diceapi.com/%sd6', $quantity));
        $result = $this->jsonResultFactory->create();
        return $result->setData(json_decode($this->curl->getBody(), true));
    }
}
