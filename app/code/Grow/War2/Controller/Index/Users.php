<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */
namespace Grow\War2\Controller\Index;

use Magento\Framework\App\ResponseInterface;

class Users extends \Magento\Framework\App\Action\Action
{

    public function execute()
    {
        // TODO: Implement execute() method.
    }

    private function getUsers()
    {
        return [
            'KZ0IL224A7NI6R6ZYBMM' => 'Jack',
            '1GUNVEXHOYLDN66YYSG5' => 'Jane',
            '80W7N61MWPA9MQ3BHI2Q' => 'Peter',
            '48LIQV6N9M9FNXHNZ99D' => 'Vanessa',
            'JZER2JIIM2ZZA6FO26YF' => 'Ana',
            '4HERTIZLH7WON5XHTJ3Y' => 'Jeffry'
        ];
    }
}
