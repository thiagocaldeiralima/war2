<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */
namespace Grow\War2\Controller\Index;

use Grow\War2\Model\GameManager;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Next extends Action
{
    /**
     * @var GameManager
     */
    private $gameManager;
    /**
     * @var JsonFactory
     */
    private $jsonFactory;

    /**
     * Next constructor.
     * @param Context $context
     * @param GameManager $gameManager
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        GameManager $gameManager,
        JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->gameManager = $gameManager;
        $this->jsonFactory = $jsonFactory;
    }

    public function execute()
    {
        $result = $this->jsonFactory->create();
        $nextPlayer = $this->gameManager->setNextPlayer();
        return $result->setData(['nextPlayer' => $nextPlayer->getData()]);
    }
}
