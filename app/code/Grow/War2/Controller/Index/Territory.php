<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */
namespace Grow\War2\Controller\Index;

use Grow\War2\Model\Card;
use Grow\War2\Model\GameManager;
use Grow\War2\Model\PlayerManager;
use Grow\War2\Model\ResourceModel\Card\Collection;
use Grow\War2\Model\ResourceModel\Card\CollectionFactory;
use Magento\Framework\App\Action\Action as ActionAlias;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class Territory extends ActionAlias
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var JsonFactory
     */
    private $jsonFactory;
    /**
     * @var GameManager
     */
    private $gameManager;
    /**
     * @var PlayerManager
     */
    private $playerManager;

    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        JsonFactory $jsonFactory,
        GameManager $gameManager,
        PlayerManager $playerManager
    ) {
        parent::__construct($context);
        $this->collectionFactory = $collectionFactory;
        $this->jsonFactory = $jsonFactory;
        $this->gameManager = $gameManager;
        $this->playerManager = $playerManager;
    }

    public function execute()
    {
        if (!$this->gameManager->isLoggedIn()) {
            $this->_redirect('*/*/login');
        }
        $response = $this->jsonFactory->create();
        $response->setData([
            'success' => true,
            'territory' => $this->playerManager->appendTerritoryCards()
        ]);
        return $response;
    }
}
