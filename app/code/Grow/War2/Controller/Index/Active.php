<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */
namespace Grow\War2\Controller\Index;

use Grow\War2\Model\GameManager;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface as ResponseInterfaceAlias;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

class Active extends \Magento\Framework\App\Action\Action
{
    /**
     * @var GameManager
     */
    private $gameManager;
    /**
     * @var JsonFactory
     */
    private $jsonResultFactory;

    public function __construct(
        Context $context,
        GameManager $gameManager,
        JsonFactory $jsonResultFactory
    ) {
        parent::__construct($context);
        $this->gameManager = $gameManager;
        $this->jsonResultFactory = $jsonResultFactory;
    }

    /**
     * @return ResponseInterfaceAlias|Json|ResultInterface
     */
    public function execute()
    {
        $result = $this->gameManager->isLoggedIn()
            ? ['success' => true, 'player' => $this->gameManager->getActivePlayer()->getData()]
            : ['error' => 'Please login in'];
        $response = $this->jsonResultFactory->create();
        return $response->setData($result);
    }
}
