<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */
namespace Grow\War2\Controller\Index;

use Grow\War2\Model\HistoryFactory;
use Grow\War2\Model\PlayerManager;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;

class Logger extends Action
{

    /**
     * @var PlayerManager
     */
    private $playerManager;
    /**
     * @var JsonFactory
     */
    private $jsonResultFactory;
    /**
     * @var HistoryFactory
     */
    private $historyFactory;
    /**
     * @var DateTime
     */
    private $date;

    public function __construct(
        Context $context,
        PlayerManager $playerManager,
        HistoryFactory $historyFactory,
        JsonFactory $jsonResultFactory,
        DateTime $date
    ) {
        parent::__construct($context);
        $this->playerManager = $playerManager;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->historyFactory = $historyFactory;
        $this->date = $date;
    }

    public function execute()
    {
        if ($content = $this->getRequest()->getParam('content')) {
            $history = $this->historyFactory->create();
            $contentAndTime = "<p>" . $this->date->gmtDate('d-m-Y: H:i:s') . ': ' . $content . "</p>";
            $history->setData('content', $contentAndTime)->save();
        }
        $response = $this->jsonResultFactory->create();
        return $response->setData([
            'success' => true
        ]);
    }
}
