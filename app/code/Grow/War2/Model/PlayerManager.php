<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */

namespace Grow\War2\Model;

class PlayerManager
{
    /**
     * @var GameManager
     */
    private $gameManager;
    /**
     * @var PlayerFactory
     */
    private $playerFactory;
    /**
     * @var CardFactory
     */
    private $cardFactory;

    /**
     * PlayerManager constructor.
     * @param GameManager $gameManager
     * @param PlayerFactory $playerFactory
     * @param CardFactory $cardFactory
     */
    public function __construct(
        GameManager $gameManager,
        PlayerFactory $playerFactory,
        CardFactory $cardFactory
    ) {
        $this->gameManager = $gameManager;
        $this->playerFactory = $playerFactory;
        $this->cardFactory = $cardFactory;
    }

    /**
     * @param $player
     */
    public function updateGameData($player)
    {
        $content = json_decode($player->getData('content') ?? '{}', true);
        if (!isset($content['objective'])) {
            $objective = $this->gameManager->getObjective();
            $content['objective'] = $objective->getData();
            $player->setData('content', json_encode($content))->save();
            $objective->setData('is_used', 1)->save();
        }
        $this->gameManager->updateGameData();
    }

    /**
     * @param null $player
     * @return array
     * @throws \Exception
     */
    public function appendTerritoryCards($player = null)
    {
        if (!$player) {
            $player = $this->gameManager->getPlayer();
        }
        $content = json_decode($player->getData('content') ?? '{}', true);
        if (!isset($content['territory'])) {
            $content['territory'] = [];
        }
        $maxCards = $this->getMaxTerritory($content['territory']);
        $territories = $this->gameManager->getTerritories($maxCards);
        foreach ($territories as $territory) {
            $content['territory'][] = $territory->getData();
            $territory->setData('is_used', 1)->save();
        }
        $player->setData('content', json_encode($content))->save();
        return $content['territory'];
    }

    /**
     * @param $cardIds
     */
    public function useTerritoryCards($cardIds)
    {
        $activePlayer = $this->gameManager->getActivePlayer();
        $loggedPlayer = $this->gameManager->getPlayer();

        if ($loggedPlayer->getId() == $activePlayer->getId()) {
            $content = json_decode($loggedPlayer->getData('content') ?? '{}', true);
            if (isset($content['territory'])) {
                $newTerritory = [];
                foreach ($content['territory'] as $key => $territory) {
                    if (!in_array($territory['id'], $cardIds)) {
                        $newTerritory[] = $territory;
                    }
                }
                $content['territory'] = $newTerritory;
                $activePlayer->setData('content', json_encode($content))->save();
                $this->gameManager->setActivePlayer($activePlayer);
                $this->gameManager->setPlayer($activePlayer);
            }
            foreach ($cardIds as $cardId) {
                $card = $this->cardFactory->create();
                $card->load($cardId);
                $card->setData('is_used', 0)->save();
            }
        }
    }

    /**
     * @param array $territory
     * @return int|void
     */
    private function getMaxTerritory(array $territory)
    {
        $playerCards = sizeof($territory);
        $totalCards = 5 - $playerCards;
        return $totalCards > 3 ? 3 : $totalCards;
    }
}
