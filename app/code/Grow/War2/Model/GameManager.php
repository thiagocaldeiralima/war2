<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */

namespace Grow\War2\Model;

use Grow\War2\Model\ResourceModel\Player\Collection as PlayerCollection;
use Grow\War2\Model\ResourceModel\Player\CollectionFactory as PlayerCollectionFactory;
use Grow\War2\Model\ResourceModel\Card\CollectionFactory as CardCollectionFactory;
use Grow\War2\Model\ResourceModel\History\CollectionFactory as HistoryCollectionFactory;
use Grow\War2\Model\ResourceModel\History\Collection as HistoryCollection;
use Grow\War2\Model\ResourceModel\Board\CollectionFactory as BoardCollectionFactory;
use Grow\War2\Model\ResourceModel\Board\Collection as BoardCollection;
use Grow\War2\Model\ResourceModel\Card\Collection as CardCollection;
use Magento\Framework\Session\SessionManagerInterface as Session;

class GameManager
{
    /**
     * @var Session
     */
    private $sessionManager;
    /**
     * @var PlayerCollectionFactory
     */
    private $playerCollectionFactory;
    /**
     * @var CardCollectionFactory
     */
    private $cardCollectionFactory;
    /**
     * @var HistoryCollectionFactory
     */
    private $historyCollectionFactory;
    /**
     * @var BoardCollectionFactory
     */
    private $boardCollectionFactory;

    /**
     * @var Player
     */
    private $activePlayer;

    public function __construct(
        Session $sessionManager,
        PlayerCollectionFactory $playerCollectionFactory,
        CardCollectionFactory $cardCollectionFactory,
        HistoryCollectionFactory $historyCollectionFactory,
        BoardCollectionFactory $boardCollectionFactory
    ) {
        $this->sessionManager = $sessionManager;
        $this->playerCollectionFactory = $playerCollectionFactory;
        $this->cardCollectionFactory = $cardCollectionFactory;
        $this->historyCollectionFactory = $historyCollectionFactory;
        $this->boardCollectionFactory = $boardCollectionFactory;
    }

    /**
     * @return bool
     */
    public function isLoggedIn()
    {
        /** @var Player $player */
        $player = $this->sessionManager->getPlayer();
        return ($player && $player->getId()) ? (bool)$player->getId() : false;
    }

    /**
     * @return array
     */
    public function getPlayerData()
    {
        if ($this->isLoggedIn()) {
            return $this->getPlayer()->getData();
        }
        return [];
    }

    /**
     * @return mixed
     */
    public function getActivePlayer()
    {
        if (!$this->activePlayer) {
            $board = $this->getGameBoard();
            /** @var PlayerCollection $collection */
            $collection = $this->playerCollectionFactory->create();
            $collection->addFieldToFilter('id', $board->getData('active_player_id'));
            $this->activePlayer = $collection->getFirstItem();
        }
        return $this->activePlayer;
    }

    /**
     * @param $activePlayer
     */
    public function setActivePlayer($activePlayer)
    {
        $board = $this->getGameBoard();
        $board->setData('active_player_id', $activePlayer->getId());
        $board->save();
    }

    /**
     * @param $player
     * @return mixed
     */
    public function setPlayer($player)
    {
        return $this->sessionManager->setPlayer($player);
    }

    /**
     * @return bool|\Magento\Framework\DataObject
     * @throws \Exception
     */
    public function setNextPlayer()
    {
        $activePlayer = $this->getActivePlayer();
        $loggedPlayer = $this->getPlayer();

        if ($activePlayer->getId() != $loggedPlayer->getId()) {
            return false;
        }

        /** @var PlayerCollection $collection */
        $collection = $this->playerCollectionFactory->create();
        $numOfPlayers = $collection->getSize();
        /** @var Player $activePlayer */

        $nextPlayerOrdering = ($activePlayer->getData('ordering') == $numOfPlayers)
            ? 1
            : $activePlayer->getData('ordering') + 1;

        $activePlayer->setData('active', 0)->save();
        $collection->addFieldToFilter('ordering', $nextPlayerOrdering);
        $nextPlayer = $collection->getFirstItem();
        $nextPlayer->setData('active', 1)->save();

        $gameBoard = $this->getGameBoard();
        $gameBoard->setData('active_player_id', $nextPlayer->getId());
        $gameBoard->setData('enemy_player_id', null);
        $gameBoard->save();
        return $nextPlayer;
    }

    public function getObjective()
    {
        /** @var CardCollection $collection */
        $collection = $this->cardCollectionFactory->create();
        $collection->addFieldToFilter('is_used', 0);
        $collection->addFieldToFilter('type', Card::TYPE_OBJECTIVE);
        $collection->setPageSize(1);
        $collection->getSelect()->orderRand();
        return $collection->getFirstItem();
    }

    /**
     * @param $maxCards
     * @return \Magento\Framework\DataObject[]
     */
    public function getTerritories($maxCards)
    {
        /** @var CardCollection $collection */
        $collection = $this->cardCollectionFactory->create();
        $collection->addFieldToFilter('is_used', 0);
        $collection->addFieldToFilter('type', Card::TYPE_TERRITORY);
        $collection->setPageSize($maxCards);
        $collection->getSelect()->orderRand();
        return $collection->getItems();
    }

    /**
     * @return array
     */
    public function getOtherPlayers()
    {
        $player = $this->getPlayer();
        /** @var PlayerCollection $collection */
        $collection = $this->playerCollectionFactory->create();
        $collection->addFieldToFilter('id', ['neq' => $player->getId()]);
        return $collection->getItems();
    }

    /**
     * @return Player|bool
     */
    public function getPlayer()
    {
        if ($this->isLoggedIn()) {
            return $this->sessionManager->getPlayer();
        }
        return false;
    }

    public function updateGameData()
    {
        if (!$this->getGameBoard() || empty($this->getGameBoard()->getData())) {
            /** @var BoardCollection $collection */
            $collection = $this->boardCollectionFactory->create();
            $collection->addFieldToFilter('id', 1); // only one game for now.
            $gameBoard = $collection->getFirstItem();

            if ($gameBoard->getData('active_player_id') == null) {
                $gameBoard->setData('active_player_id', $this->getActivePlayerFromDb()->getId());
            }
            $gameBoard->save();
        }
    }

    public function getActivePlayerFromDb()
    {
        $collection = $this->playerCollectionFactory->create();
        $collection->addFieldToFilter(['is_active', 'ordering'], [
            ['eq' => 1],
            ['eq' => 1]
        ]);
        return $collection->getFirstItem();
    }

    /**
     * @return array
     */
    public function getGameHistory()
    {
        /** @var  HistoryCollection $collection */
        $collection = $this->historyCollectionFactory->create();
        $content = "";
        $result = ['size' => $collection->getSize()];
        foreach ($collection as $history) {
            $content .= $history->getContent();
        }
        $result['content'] = $content;
        return $result;
    }

    public function setEnemy($enemyId)
    {
        $activePlayer = $this->getActivePlayer();
        $loggedPlayer = $this->getPlayer();

        if ($activePlayer->getId() != $loggedPlayer->getId()) {
            return;
        }

        $gameBoard = $this->getGameBoard();
        $gameBoard->setData('enemy_player_id', $enemyId);
        $gameBoard->save();
    }

    public function getEnemyId()
    {
        return $this->getGameBoard()->getData('enemy_player_id');
    }

    public function getEnemy()
    {
        /** @var PlayerCollection $collection */
        $collection = $this->playerCollectionFactory->create();
        return $collection->addFieldToFilter('id', $this->getEnemyId())->getFirstItem();
    }

    public function getGameBoard()
    {
        /** @var BoardCollection $collection */
        $collection = $this->boardCollectionFactory->create();
        return $collection->getFirstItem();
    }
}
