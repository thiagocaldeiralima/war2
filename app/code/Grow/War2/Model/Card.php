<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */
namespace Grow\War2\Model;

use Magento\Framework\Model\AbstractModel;

class Card extends AbstractModel
{
    const TYPE_OBJECTIVE = 1;
    const TYPE_TERRITORY = 2;

    public function _construct()
    {
        $this->_init('Grow\War2\Model\ResourceModel\Card');
    }
}
