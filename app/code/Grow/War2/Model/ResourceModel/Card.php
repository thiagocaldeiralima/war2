<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */
namespace Grow\War2\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Card extends AbstractDb
{
    public function _construct()
    {
        $this->_init('war_card','id');
    }
}
