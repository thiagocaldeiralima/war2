<?php
/**
 * @author Trellis Team
 * @copyright Copyright © Trellis (https://www.trellis.co)
 */
namespace Grow\War2\Model;

use Magento\Framework\Model\AbstractModel;

class History extends AbstractModel
{
    public function _construct()
    {
        $this->_init('Grow\War2\Model\ResourceModel\History');
    }
}
